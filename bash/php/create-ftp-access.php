<?php
require 'bootstrap.php';

$uid			= $argv[1];
$gid			= $argv[2];
$username 		= $argv[3];
$encryptedPassword 	= $argv[4];

$sql = "INSERT INTO ftpuser (id, userid, passwd, uid, gid, homedir, shell, count, accessed, modified)
 VALUES (NULL, '{$username}', '{$encryptedPassword}', '{$uid}', '{$gid}', '/home/{$username}', '/sbin/nologin', '0', current_timestamp(), current_timestamp());";

if ($conn->query($sql) === TRUE) {
  echo "The ftpuser {$username} was created.";
} else {
  echo "Error: " . $sql . "\n" . $conn->error;
}
